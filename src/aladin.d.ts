// Aladin SkyView Lite type definitions

declare module "aladin-lite" {
  // Complete API reference
  // http://aladin.cds.unistra.fr/AladinLite/doc/API/

  export type AladinOptions = {
    target: string; // "0 +0"
    cooFrame: "ICRS" | "ICRSd" | "galactic"; // "ICRS"
    survey: string; // "P/DSS2/color"
    fov: string; // 60
    showReticle: boolean; // true
    showCooGrid: boolean; // false
    showCooGridControl: boolean; // false
    projection: string; // SIN (Possible values are SIN, AIT, MOL, ARC, TAN, among others)
    showProjectionControl: boolean; // true
    showZoomControl: boolean; // true
    showFullscreenControl: boolean; // true
    showLayersControl: boolean; // true
    expandLayersControl: boolean; // false
    showGotoControl: boolean; // true
    showShareControl: boolean; // false
    showSimbadPointerControl: boolean; // false
    showContextMenu: boolean; // false
    showFrame: boolean; // true
    fullScreen: boolean; // false
    recicleColor: string; // "rgb(178, 50, 178)"
    reticleSize: number; // 22
  };

  type ALADIN_AVAILABLE_CALLBACKS =
    | "select"
    | "objectClicked"
    | "objectHovered"
    | "footprintClicked"
    | "footprintHovered"
    | "positionChanged"
    | "zoomChanged"
    | "click"
    | "mouseMove"
    | "fullScreenToggled"
    | "catalogReady";

  type ALADIN_SHAPE_STRING =
    | "circle"
    | "plus"
    | "rhomb"
    | "cross"
    | "triangle"
    | "square";

  export type ALADIN_SUPPORTED_IMAGE_FORMATS = "jpg" | "png" | "fits" | "webp"; // "jpg" is converted to "jpeg"...

  export interface ImageSurvey {
    id: string;
    name: string;
    readonly properties: {
      url: string;
      maxOrder: number;
      frame: unknown;
      tileSize: number;
      formats: ALADIN_SUPPORTED_IMAGE_FORMATS;
      minCutout: unknown;
      maxCutout: unknown;
      bitpix: unknown;
      skyFraction: unknown;
      minOrder: unknown;
      hipsInitialFov: unknown;
      hipsInitialRa: number;
      hipsInitialDec: number;
      dataproductSubtype: unknown;
      isPlanetaryBody: boolean;
      hipsBody: unknown;
    };
    readonly isReady: () => boolean;
    readonly setImageFormat: (format: ALADIN_SUPPORTED_IMAGE_FORMATS) => void;
    readonly setOpacity: (opacity: number) => void;
  }

  export interface AladinInstance {
    /**
     * returns a [ra, dec] with the current equatorial coordinates of
     * the Aladin Lite view center
     */
    readonly getRaDec: () => [number, number];

    /**
     * will return an array with the current dimension (width, height)
     * of Aladin Lite view in pixels
     */
    readonly getSize: () => [number, number];

    /**
     * returns an array with the current dimension on the sky
     * (size in X, size in Y) of the view in decimal degrees
     */
    readonly getFov: () => [number, number];

    /**
     * Set the FoV size
     * @param fov Field of view in degress
     */
    readonly setFov: (fov: number) => void;

    /**
     * Sets the center coordinates of the sky view
     * @param ra Right ascension in degrees
     * @param dec Declination in degrees
     */
    readonly gotoRaDec: (ra: number, dec: number) => void;

    readonly on: (
      event: ALADIN_AVAILABLE_CALLBACKS,
      handler: (evt: unknown) => void
    ) => void;

    readonly addCatalog: (catalog: CatalogInstance) => void;
    readonly removeLayers: () => void; // remove all catalogs and overlays
    readonly createImageSurvey: (
      hips_id: string,
      hips_name: string,
      hips_base_url: string,
      hips_frame: null,
      hips_max_order: null,
      options: { imgFormat: ALADIN_SUPPORTED_IMAGE_FORMATS }
    ) => ImageSurvey;
    readonly newImageSurvey: (id: string) => ImageSurvey;
    readonly setBaseImageLayer: (imageSurvey: ImageSurvey) => void;
  }

  export type CatalogOptions = {
    name: string; // catalog label
    shape: ALADIN_SHAPE_STRING | HTMLImageElement | HTMLCanvasElement;
    color: string; // hex string?
    sourceSize: number; // size in pixels
    raField: string | number; // ID, name or index of the field used for Right Ascension
    decField: string | number; // ID, name or index of the field used for declination
    labelColumn: string | number;
    labelColor: string;
    labelFont: string; // e.g. `12px sans-serif`
    onClick: undefined | "showTable" | "showPopup";
    limit: undefined | number; // limit of no. of sources
  };

  export interface AladinCatalog {
    readonly addSources: (src: AladinSource | AladinSource[]) => void;
    readonly remove: (src: AladinSource) => void;
  }

  export type AladinSource = {
    ra: number;
    dec: number;
    data: opbject;
  };

  export interface Aladin {
    readonly init: Promise<void>;
    readonly aladin: (
      id: string,
      option?: Partial<AladinOptions>
    ) => AladinInstance;
    readonly catalog: (option?: Partial<CatalogOptions>) => AladinCatalog;
    readonly source: (ra: number, dec: number, data?: object) => AladinSource;
  }

  const A: Aladin;
  export default A;
}
