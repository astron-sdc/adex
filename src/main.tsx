/**
 *  Copyright 2023 ASTRON
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import React from "react";
import ReactDOM from "react-dom/client";
import {
  RouteObject,
  RouterProvider,
  createBrowserRouter,
} from "react-router-dom";

import Api from "./api/Api";
import LandingPage from "./pages/LandingPage/LandingPage";
import ProcessDetailPage from "./pages/ProcessDetailPage/ProcessDetailPage";

import Root from "./components/Root/Root";
import RootError from "./components/RootError/RootError";

import "@astron-sdc/design-system/styles.css";
import "./fonts.css";
import "./index.css";
import PrimaryDataProductDetailPage from "./pages/PrimaryDataProductDetailPage/PrimaryDataProductDetailPage";

const routes: RouteObject[] = [
  {
    path: "/",
    element: <Root />,
    errorElement: <RootError />,
    children: [
      {
        path: "/",
        element: <LandingPage />,
      },
      {
        path: "/process/:processId",
        loader: ({ params }) => Api.getActivity(params.processId),
        element: <ProcessDetailPage />,
      },
      {
        path: "/dataproduct/:dataProductId",
        loader: ({ params }) =>
          Api.getPrimaryDataProduct(Number(params.dataProductId)),
        element: <PrimaryDataProductDetailPage />,
      },
    ],
  },
];

const router = createBrowserRouter(routes, { basename: "/adex-next" });

const rootElement = document.getElementById("root") as HTMLElement;
const root = ReactDOM.createRoot(rootElement);
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
