interface PrimaryDataProductParams {
  collection: string;
  dp_type: string;
  ra: number;
  dec: number;
  fov: number;
  distinct_field: string;
  dataset_id?: string;
}

export default PrimaryDataProductParams;
