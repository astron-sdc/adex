interface ApiListResponse<T> {
  description: string;
  version: string;
  page: string;
  page_size: number;
  default_page_size: number;
  max_page_size: number;
  count: number;
  pages: number;
  links: { previous: string; next: string };
  results: T[];
}

export default ApiListResponse;
