import Collection from "./Collection";
import SkyViewConfiguration from "./SkyViewConfiguration";

interface ConfigurationBody {
  collections: Collection[];
  skyview: SkyViewConfiguration;
}

export default ConfigurationBody;
