import SurveyConfiguration from "./SurveyConfiguration";

export interface SkyViewConfiguration {
  surveys: { [key: string]: SurveyConfiguration[] };
}

export default SkyViewConfiguration;
