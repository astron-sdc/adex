interface Collection {
  name: string;
  dp_types: string[];
  distinct_field?: string;
}

export default Collection;
