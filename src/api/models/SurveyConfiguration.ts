import { ALADIN_SUPPORTED_IMAGE_FORMATS } from "aladin-lite";

export interface SurveyConfiguration {
  id: string;
  name: string;
  url: string;
  format: ALADIN_SUPPORTED_IMAGE_FORMATS;
}

export default SurveyConfiguration;
