export interface SkyCoordinates {
  description: string;
  target_name: string;
  ra: string;
  dec: string;
}

export interface SkyCoordinatesError {
  error: string;
}

type GetSkyCoordinates = SkyCoordinates | SkyCoordinatesError;

export default GetSkyCoordinates;
