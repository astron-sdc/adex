import ConfigurationBody from "./ConfigurationBody";

interface Configuration {
  version: string;
  config: ConfigurationBody;
}

export default Configuration;
