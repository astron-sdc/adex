interface Activity {
  id: number;
  name: string;
  type: string;
  url: string;
  version: string;
  collection: string;
  ra: number;
  dec: number;
  parents: Activity[];
}

export default Activity;
