interface PrimaryDataProduct {
  id: number;
  pid: string;
  dp_type: string;
  ra: number;
  dec: number;
  activity: string;
  access_url: string;
}

export default PrimaryDataProduct;
