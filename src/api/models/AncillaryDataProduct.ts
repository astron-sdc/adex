interface AncillaryDataProduct {
  pid: string;
  dp_type: string;
}

export default AncillaryDataProduct;
