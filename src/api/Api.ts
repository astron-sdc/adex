import { fetchAndReadJson } from "../utils/Fetch";
import { buildUriFromParams } from "../utils/Uri";
import ApiListResponse from "./ApiListResponse";
import ConfigurationResponse from "./ConfigurationResponse";
import Activity from "./models/Activity";
import AncillaryDataProduct from "./models/AncillaryDataProduct";
import PrimaryDataProduct from "./models/PrimaryDataProduct";
import PrimaryDataProductParams from "./params/PrimaryDataProductParams";
import GetSkyCoordinates from "./models/GetSkyCoordinates";

class Api {
  async getConfiguration(): Promise<ConfigurationResponse> {
    const uri = `/adex_backend/configuration/?config_name=adex-next`;
    const response = await fetch(uri);
    const json = await response.json();
    return json;
  }

  async getPrimaryDataProducts(
    params: Partial<PrimaryDataProductParams>
  ): Promise<ApiListResponse<PrimaryDataProduct>> {
    const uri = buildUriFromParams("/adex_backend/api/v1/primary_dp/?", params);
    return fetchAndReadJson(uri);
  }

  async getPrimaryDataProduct(
    dataProductId?: number
  ): Promise<PrimaryDataProduct> {
    if (!dataProductId) {
      throw TypeError(`Invalid dataProductId: ${dataProductId}`);
    }

    const uri = `/adex_backend/api/v1/primary_dp/${dataProductId}`;
    return fetchAndReadJson(uri);
  }

  async getAncillaryDataProducts(): Promise<AncillaryDataProduct[]> {
    return Promise.resolve([]);
  }

  /**
   * Get a single Activity (process) by activity id
   * @param activityId id
   * @returns an Activity
   */
  async getActivity(activityId?: string): Promise<Activity> {
    if (!activityId) {
      throw TypeError(`Invalid activityId: ${activityId}`);
    }

    const uri = `/adex_backend/api/v1/activity/${activityId}/`;
    return fetchAndReadJson(uri);
  }

  async getCoordsByTargetName(targetName: string): Promise<GetSkyCoordinates> {
    const uri = buildUriFromParams("/adex_backend/get-sky-coordinates/", {
      target_name: targetName,
    });
    return fetchAndReadJson(uri);
  }
}

export default new Api();
