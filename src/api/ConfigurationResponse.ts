import Configuration from "./models/Configuration";

interface ConfigurationResponse {
  configuration: Configuration;
}

export default ConfigurationResponse;
