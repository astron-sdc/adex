import { filterEmptyAndWhitespace } from "../../utils/Array";
import { downloadFromUrl } from "../../utils/Download";
import PrimaryDataProduct from "../models/PrimaryDataProduct";

async function downloadPrimaryDataProducts(
  dataProducts: PrimaryDataProduct[],
  document: Document
) {
  const access_urls = dataProducts.map((dataproduct) => dataproduct.access_url);

  for (const access_url of filterEmptyAndWhitespace(access_urls)) {
    downloadFromUrl(document, access_url);
  }
}

export { downloadPrimaryDataProducts };
