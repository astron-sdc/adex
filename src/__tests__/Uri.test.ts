import { buildUriFromParams } from "../utils/Uri";

describe("Uri", () => {
  it("should return the base uri when params has no keys", () => {
    const baseUri = "http://birdwatching.com";
    const expected = "http://birdwatching.com?";
    const result = buildUriFromParams(baseUri, {});
    expect(result).toBe(expected);
  });

  it("should not include '&' before the first value", () => {
    const baseUri = "http://birdwatching.com?";
    const expected = "http://birdwatching.com?birdType=magpie&size=25";
    const result = buildUriFromParams(baseUri, {
      birdType: "magpie",
      size: 25,
    });
    expect(result).toBe(expected);
  });

  it("should include null values when includeNullUndefinedAndEmpty is true", () => {
    const baseUri = "http://birdwatching.com?";
    const expected = "http://birdwatching.com?birdType=null";
    const result = buildUriFromParams(baseUri, { birdType: null }, true);
    expect(result).toBe(expected);
  });

  it("should include undefined values when includeNullUndefinedAndEmpty is true", () => {
    const baseUri = "http://birdwatching.com?";
    const expected = "http://birdwatching.com?birdType=undefined";
    const result = buildUriFromParams(baseUri, { birdType: undefined }, true);
    expect(result).toBe(expected);
  });

  it("should include empty values when includeNullUndefinedAndEmpty is true", () => {
    const baseUri = "http://birdwatching.com?";
    const expected = "http://birdwatching.com?birdType=";
    const result = buildUriFromParams(baseUri, { birdType: "" }, true);
    expect(result).toBe(expected);
  });

  it("should not include falsy values besides 0 when includeNullUndefinedAndEmpty is false", () => {
    const baseUri = "http://birdwatching.com?";
    const expected = "http://birdwatching.com?size=10&season=winter";
    const result = buildUriFromParams(baseUri, {
      birdType: "",
      size: 10,
      season: "winter",
    });
    expect(result).toBe(expected);
  });

  it("should include 0 regardless, even when includeNullUndefinedAndEmpty is false", () => {
    const baseUri = "http://birdwatching.com?";
    const expected = "http://birdwatching.com?size=0&season=winter";
    const result = buildUriFromParams(baseUri, {
      size: 0,
      season: "winter",
    });
    expect(result).toBe(expected);
  });

  it("should include 0 regardles, when includeNullUndefinedAndEmpty is true", () => {
    const baseUri = "http://birdwatching.com?";
    const expected = "http://birdwatching.com?size=0&season=winter";
    const result = buildUriFromParams(
      baseUri,
      {
        size: 0,
        season: "winter",
      },
      true
    );
    expect(result).toBe(expected);
  });

  it("should encode parameters", () => {
    const baseUri = "http://birdwatching.com?";
    const expected = "http://birdwatching.com?season=winter+wonder";
    const result = buildUriFromParams(baseUri, {
      season: "winter wonder",
    });
    expect(result).toBe(expected);
  });

  it("should add a question mark to the base url when it's not present", () => {
    const baseUri = "http://birdwatching.com";
    const expected = "http://birdwatching.com?season=summer";
    const result = buildUriFromParams(baseUri, {
      season: "summer",
    });
    expect(result).toBe(expected);
  });

  it("should be able to handle empty base urls", () => {
    const baseUri = "";
    const expected = "?season=summer";
    const result = buildUriFromParams(baseUri, {
      season: "summer",
    });
    expect(result).toBe(expected);
  });
});
