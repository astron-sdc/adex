/**
 *  Copyright 2023 ASTRON
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { render } from "@testing-library/react";

import * as ReactRouter from "react-router";
import { vi } from "vitest";
import RootError from "../components/RootError/RootError";

describe("Root", async () => {
  it("renders errors raised by ReactRouter", async () => {
    vi.spyOn(ReactRouter, "useRouteError").mockImplementation(
      vi.fn().mockReturnValue({
        // There are other fields, but RootError only checks `error`
        error: Error("Invalid path"),
      })
    );

    const container = render(<RootError />);
    expect(container.queryByTestId("errorMessage")).toBeInTheDocument();
  });

  it("renders errors raised by other code", async () => {
    vi.spyOn(ReactRouter, "useRouteError").mockImplementation(
      vi.fn().mockReturnValue(Error("FooBar!"))
    );

    const container = render(<RootError />);
    expect(container.queryByTestId("errorMessage")).toBeInTheDocument();
  });
});
