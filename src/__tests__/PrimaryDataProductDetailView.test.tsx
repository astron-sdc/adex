import { render } from "@testing-library/react";
import PrimaryDataProduct from "../api/models/PrimaryDataProduct";
import { MemoryRouter } from "react-router-dom";
import PrimaryDataProductDetailView from "../components/DataProduct/PrimaryDataProductDetailView";

describe("PrimaryDataProductDetailView", async () => {
  it("renders correctly", async () => {
    const primaryDataProduct = {
      id: 1,
    } as PrimaryDataProduct;

    const container = render(
      <MemoryRouter>
        <PrimaryDataProductDetailView primaryDataProduct={primaryDataProduct} />
      </MemoryRouter>
    );

    expect(container).toBeTruthy();
  });
});
