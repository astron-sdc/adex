import { render } from "@testing-library/react";
import Root from "../components/Root/Root";
import { MemoryRouter } from "react-router-dom";

describe("Root", async () => {
  it("renders correctly", async () => {
    // root expects a router, so wrap it in a lightweight MemoryRouter for testing purposes
    const container = render(
      <MemoryRouter>
        <Root />
      </MemoryRouter>
    );
    expect(container).toBeTruthy();
  });
});
