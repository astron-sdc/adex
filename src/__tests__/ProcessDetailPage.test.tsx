/**
 *  Copyright 2023 ASTRON
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { render } from "@testing-library/react";
import * as ReactQuery from "react-query";
import * as ReactRouter from "react-router";
import { vi } from "vitest";
import Activity from "../api/models/Activity";
import ProcessDetailPage from "../pages/ProcessDetailPage/ProcessDetailPage";

describe("ProcessDetailPage", async () => {
  it("renders correctly with an existing activity and no data products", async () => {
    vi.spyOn(ReactRouter, "useLoaderData").mockImplementation(
      vi.fn<unknown[], Activity>().mockReturnValue({
        id: 0,
        name: "ABC",
        type: "type",
        url: "https://example.com",
        version: "alpha",
        collection: "cards",
        ra: 0,
        dec: 0,
        parents: [],
      })
    );

    vi.spyOn(ReactQuery, "useQuery").mockImplementation(
      vi.fn().mockReturnValue({
        loading: false,
        data: [],
      })
    );

    const container = render(<ProcessDetailPage />);
    expect(container).toBeTruthy();
  });

  it("renders correctly with an existing activity and loading data products", async () => {
    vi.spyOn(ReactRouter, "useLoaderData").mockImplementation(
      vi.fn<unknown[], Activity>().mockReturnValue({
        id: 0,
        name: "ABC",
        type: "type",
        url: "https://example.com",
        version: "alpha",
        collection: "cards",
        ra: 0,
        dec: 0,
        parents: [],
      })
    );

    vi.spyOn(ReactQuery, "useQuery").mockImplementation(
      vi.fn().mockReturnValue({
        loading: true,
        data: null,
      })
    );

    const container = render(<ProcessDetailPage />);
    expect(container.findByTestId("loading")).toBeTruthy();
  });
});
