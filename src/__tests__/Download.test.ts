import { downloadFromUrl } from "../utils/Download";
import { vi } from "vitest";
import { JSDOM } from "jsdom";

describe("Download", () => {
  describe("downloadFromUrl", () => {
    it("should append an iframe with the correct URL to the DOM", () => {
      const dom = new JSDOM();
      const document = dom.window.document;
      const spy = vi.spyOn(document.body, "appendChild");

      downloadFromUrl(dom.window.document, "www.disney.com");

      const element = spy.mock.calls[0][0] as HTMLIFrameElement;
      expect(element.src).toBe("www.disney.com");
    });
  });
});
