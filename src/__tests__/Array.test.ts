import { filterEmptyAndWhitespace } from "../utils/Array";

describe("Array", () => {
  describe("filterEmptyAndWhitespace", () => {
    it("should filter empty strings", () => {
      const array = ["1", "", "2", "", "3"];
      const expected = ["1", "2", "3"];
      expect(filterEmptyAndWhitespace(array)).toStrictEqual(expected);
    });

    it("should filter whitespace strings", () => {
      const array = ["1", "  ", "2", "      ", "3"];
      const expected = ["1", "2", "3"];
      expect(filterEmptyAndWhitespace(array)).toStrictEqual(expected);
    });
  });
});
