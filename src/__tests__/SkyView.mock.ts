import {
  Aladin,
  AladinCatalog,
  AladinInstance,
  AladinOptions,
  AladinSource,
  CatalogOptions,
  ImageSurvey,
} from "aladin-lite";

/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-unused-vars */

const DUMMY_IMAGE_SURVEY: ImageSurvey = {
  id: "",
  name: "",
  properties: {
    url: "",
    maxOrder: 0,
    tileSize: 0,
    formats: "png",
    hipsInitialRa: 0,
    hipsInitialDec: 0,
    isPlanetaryBody: false,
    frame: null,
    minCutout: null,
    maxCutout: null,
    bitpix: null,
    skyFraction: null,
    minOrder: null,
    hipsInitialFov: null,
    dataproductSubtype: null,
    hipsBody: null,
  },
  isReady: () => true,
  setImageFormat: (_0) => {},
  setOpacity: (_0) => {},
};

export const MockAladinInstance: AladinInstance = {
  getRaDec: () => [0, 0],
  getFov: () => [60, 40],
  gotoRaDec: (_0, _1) => {},
  setFov: (_) => {},
  getSize: () => [0, 0],
  on: (_0, _1) => {},
  addCatalog: (_0) => {},
  removeLayers: () => {},
  createImageSurvey: (_0, _1, _2, _3, _4, { imgFormat: _5 }) =>
    DUMMY_IMAGE_SURVEY,
  newImageSurvey: (_0) => DUMMY_IMAGE_SURVEY,
  setBaseImageLayer: (_0) => {},
};

class TestAladin implements Aladin {
  private _init: Promise<void>;

  constructor(resolves = false) {
    if (resolves) {
      this._init = Promise.resolve();
    } else {
      this._init = new Promise(() => {});
    }
  }

  public get init(): Promise<void> {
    return this._init;
  }

  public aladin(
    element_id: string,
    _?: Partial<AladinOptions>
  ): AladinInstance {
    // Remove the # from the ID (this is not jquery...)
    const el = document.getElementById(element_id.substring(1));
    if (!el) throw Error("Could not find element in test");
    const cvs = document.createElement("canvas");
    cvs.className = "aladin-catalogCanvas";
    el.appendChild(cvs);
    const zoomPlus = document.createElement("div");
    zoomPlus.className = "zoomPlus";
    el.appendChild(zoomPlus);
    const zoomMinus = document.createElement("div");
    zoomMinus.className = "zoomMinus";
    el.appendChild(zoomMinus);

    return MockAladinInstance;
  }

  public catalog(_?: Partial<CatalogOptions> | undefined): AladinCatalog {
    return {
      addSources: (_) => {},
      remove: (_) => [],
    };
  }

  public source(ra: number, dec: number, data?: object): AladinSource {
    return { ra: ra, dec: dec, data: data };
  }
}

/* eslint-enable @typescript-eslint/no-empty-function */
/* eslint-enable @typescript-eslint/no-unused-vars */

export const AladinStub = new TestAladin();
export const AladinStubResolves = new TestAladin(true);
