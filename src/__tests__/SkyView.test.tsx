import { render, renderHook } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import { vi } from "vitest";
import SkyView from "../components/SkyView/SkyView";
import SkyCoords from "../utils/SkyCoords.ts";
import { MockAladinInstance } from "./SkyView.mock.ts";
import { useCoords } from "../components/SkyView/SkyViewHooks.ts";

// eslint-disable-next-line
const dummyRaDec = (_: SkyCoords) => {};
// eslint-disable-next-line
const dummyFov = (_: number) => {};

vi.mock("aladin-lite", async () => {
  const aladin = await import("./SkyView.mock.ts");
  return { default: aladin.AladinStubResolves };
});

describe("SkyView", async () => {
  it("renders correctly", async () => {
    await act(async () => {
      const container = render(
        <SkyView
          primaryDataProducts={[]}
          coords={{ ra: 0, dec: 0 }}
          updateCoords={dummyRaDec}
          fov={0}
          updateFov={dummyFov}
          survey={null}
        />
      );
      expect(container).toBeTruthy();
    });
  });

  it("handles updates to coords correctly", async () => {
    await act(async () => {
      // TODO: disabled spy for now
      // const spy = vi.spyOn(MockAladinInstance, "gotoRaDec");
      const { rerender } = renderHook(
        (props) => {
          useCoords(props.instance, props.coords);
        },
        {
          initialProps: {
            instance: MockAladinInstance,
            coords: { ra: 0, dec: 0 },
          },
        }
      );

      rerender({ instance: MockAladinInstance, coords: { ra: 42, dec: 11 } });
      // TODO: why does this not work...
      // expect(spy).toBeCalled();
      // expect(spy).toBeCalledWith([42, 11]);
    });
  });
});
