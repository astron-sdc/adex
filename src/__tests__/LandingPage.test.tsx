// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
// TODO: fix typing when mocking useQuery

import { render } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import * as ReactQuery from "react-query";
import { QueryClient, QueryClientProvider } from "react-query";
import { vi } from "vitest";
import createFetchMock from "vitest-fetch-mock";
import ApiListResponse from "../api/ApiListResponse";
import ConfigurationResponse from "../api/ConfigurationResponse";
import PrimaryDataProduct from "../api/models/PrimaryDataProduct";
import LandingPage from "../pages/LandingPage/LandingPage";
import { createFetchResponse } from "./utils";

const fetchMocker = createFetchMock(vi);

vi.mock("aladin-lite", async () => {
  const aladin = await import("./SkyView.mock.ts");
  return { default: aladin.AladinStubResolves };
});

describe("LandingPage", async () => {
  beforeAll(() => {
    fetchMocker.enableMocks();
  });

  beforeEach(() => {
    fetchMocker.resetMocks();
  });

  it("renders correctly", async () => {
    vi.spyOn(ReactQuery, "useQuery").mockImplementation(
      vi.fn().mockReturnValue({
        data: {},
        isLoading: true,
      })
    );

    const container = render(
      <QueryClientProvider client={new QueryClient()}>
        <LandingPage />
      </QueryClientProvider>
    );
    expect(container).toBeTruthy();
  });

  it("shows a filter loader when loading filters", async () => {
    vi.spyOn(ReactQuery, "useQuery").mockImplementation(
      vi.fn().mockReturnValue({
        data: {},
        isLoading: true,
      })
    );

    const container = render(
      <QueryClientProvider client={new QueryClient()}>
        <LandingPage />
      </QueryClientProvider>
    );

    expect(container.queryByTestId("filterLoader")).toBeInTheDocument();
  });

  it("shows a data product loader when loading data products", async () => {
    vi.spyOn(ReactQuery, "useQuery").mockImplementation(
      vi.fn().mockReturnValue({
        data: {},
        isLoading: true,
      })
    );

    const container = render(
      <QueryClientProvider client={new QueryClient()}>
        <LandingPage />
      </QueryClientProvider>
    );

    expect(container.queryByTestId("dataProductLoader")).toBeInTheDocument();
  });

  it("does not show a filter loader when not loading filters", async () => {
    vi.spyOn(ReactQuery, "useQuery").mockImplementation(
      vi.fn().mockReturnValue({
        data: [],
        isLoading: false,
      })
    );

    const container = render(
      <QueryClientProvider client={new QueryClient()}>
        <LandingPage />
      </QueryClientProvider>
    );

    expect(container.queryByTestId("filterLoader")).not.toBeInTheDocument();
  });

  it("does not show a data product loader when not loading data products", async () => {
    vi.spyOn(ReactQuery, "useQuery").mockImplementation(
      vi.fn().mockReturnValue({
        data: [],
        isLoading: false,
      })
    );

    const container = render(
      <QueryClientProvider client={new QueryClient()}>
        <LandingPage />
      </QueryClientProvider>
    );

    expect(container.queryByTestId("dataProductLoader")).not.toBeVisible();
  });

  it("uses the correct data returned from the getConfiguration Api call", async () => {
    const configurationResponseMock: ConfigurationResponse = {
      configuration: {
        version: "ADEX-backend - version 28 April 2023",
        config: {
          collections: [
            { name: "linc_skymap", dp_types: ["qa-skymap"] },
            {
              name: "linc_visibilities",
              dp_types: ["die-calibrated-visibilities"],
              distinct_field: "dataset_id",
            },
            { name: "apertif-dr1", dp_types: ["science-skymap"] },
            { name: "lotts-dr2", dp_types: ["skymap"] },
          ],
          skyview: {
            surveys: {
              test: [
                {
                  id: "test",
                  name: "test",
                  url: "https://example.com",
                  format: "png",
                },
              ],
            },
          },
        },
      },
    };
    fetchMocker.mockResponse(
      createFetchResponse<ConfigurationResponse>(configurationResponseMock, 200)
    );

    const spy = vi.spyOn(ReactQuery, "useQuery");

    render(
      <QueryClientProvider client={new QueryClient()}>
        <LandingPage />
      </QueryClientProvider>
    );

    const collectionsSpy = spy.mock.calls.find((params) => {
      return params[0][0] === "configuration";
    });

    await act(async () => {
      const queryFunction = collectionsSpy[1];
      const result = await queryFunction();

      expect(result).toEqual(configurationResponseMock.configuration.config);
    });
  });

  it("uses the correct data returned from the getPrimaryDataProducts Api call", async () => {
    const primaryDataProductResonseMock: ApiListResponse<PrimaryDataProduct> = {
      results: [
        { pid: "1" } as PrimaryDataProduct,
        { pid: "2" } as PrimaryDataProduct,
      ],
    };

    fetchMocker.mockResponse(
      createFetchResponse<ApiListResponse<PrimaryDataProduct>>(
        primaryDataProductResonseMock,
        200
      )
    );

    const spy = vi.spyOn(ReactQuery, "useQuery");

    render(
      <QueryClientProvider client={new QueryClient()}>
        <LandingPage />
      </QueryClientProvider>
    );

    const collectionsSpy = spy.mock.calls.find((params) => {
      return (
        params[0].queryKey && params[0].queryKey[0] === "primaryDataProducts"
      );
    });

    await act(async () => {
      const queryFunction = collectionsSpy[0].queryFn;
      const result = await queryFunction();
      expect(result).toEqual(primaryDataProductResonseMock.results);
    });
  });
});
