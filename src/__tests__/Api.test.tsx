import { vi } from "vitest";
import createFetchMock from "vitest-fetch-mock";
import Api from "../api/Api";
import ApiListResponse from "../api/ApiListResponse";
import ConfigurationResponse from "../api/ConfigurationResponse";
import Activity from "../api/models/Activity";
import PrimaryDataProduct from "../api/models/PrimaryDataProduct";
import PrimaryDataProductParams from "../api/params/PrimaryDataProductParams";
import { createFetchResponse } from "./utils";

const fetchMocker = createFetchMock(vi);

describe("Api", async () => {
  beforeAll(() => {
    fetchMocker.enableMocks();
  });

  beforeEach(() => {
    fetchMocker.resetMocks();
  });

  describe("getConfiguration", async () => {
    it("should return something truthy", async () => {
      fetchMocker.mockResponse(
        createFetchResponse<ConfigurationResponse>(
          {
            configuration: {
              version: "ADEX-backend - version 28 April 2023",
              config: {
                collections: [
                  { name: "linc_skymap", dp_types: ["qa-skymap"] },
                  {
                    name: "linc_visibilities",
                    dp_types: ["die-calibrated-visibilities"],
                    distinct_field: "dataset_id",
                  },
                  { name: "apertif-dr1", dp_types: ["science-skymap"] },
                  { name: "lotts-dr2", dp_types: ["skymap"] },
                ],
                skyview: {
                  surveys: {
                    SDC_Data: [
                      {
                        id: "Some ID",
                        name: "Hello World",
                        url: "https://example.com",
                        format: "png",
                      },
                    ],
                  },
                },
              },
            },
          },
          200
        )
      );

      const response = await Api.getConfiguration();
      expect(response.configuration.config.collections).toBeTruthy();
    });
  });

  describe("getPrimaryDataProducts", async () => {
    it("should return something truthy", async () => {
      fetchMocker.mockResponse(
        createFetchResponse<ApiListResponse<PrimaryDataProduct>>(
          {
            results: [
              {
                id: 1,
                pid: "A123",
                dp_type: "Winning lottery ticket",
                ra: 0,
                dec: 0,
                activity: "5",
                access_url: "disney.com",
              },
            ],
            description: "",
            version: "",
            page: "",
            page_size: 0,
            default_page_size: 0,
            max_page_size: 0,
            count: 0,
            pages: 0,
            links: {
              previous: "",
              next: "",
            },
          },
          200
        )
      );

      const params: Partial<PrimaryDataProductParams> = {
        collection: "",
        dp_type: "",
        dec: 0,
        ra: 0,
        fov: 5,
        distinct_field: "",
        dataset_id: "",
      };

      const primaryDataProducts = await Api.getPrimaryDataProducts(params);
      expect(primaryDataProducts).toBeTruthy();
    });
  });

  describe("getPrimaryDataProduct", async () => {
    it("should return something truthy on a succesful call", async () => {
      fetchMocker.mockResponse(
        createFetchResponse<PrimaryDataProduct>(
          {
            id: 1,
            activity: "fun",
            dec: 3.14,
            ra: 2.71,
            dp_type: "steel",
            pid: "compression",
          } as PrimaryDataProduct,
          200
        )
      );

      const primaryDataProduct = await Api.getPrimaryDataProduct(1);
      expect(primaryDataProduct).toBeTruthy();
    });

    it("should throw an error on a non succesful call", async () => {
      fetchMocker.mockResponse(
        createFetchResponse<object>(
          {
            detail: "Kaboom",
          },
          500
        )
      );

      expect(Api.getPrimaryDataProduct(1)).rejects.toThrowError();
    });

    it("should throw an error when passed an invalid dataProductId", async () => {
      expect(Api.getPrimaryDataProduct(undefined)).rejects.toEqual(
        TypeError("Invalid dataProductId: undefined")
      );
    });
  });

  describe("getAncillaryDataProducts", async () => {
    it("should return something truthy", async () => {
      const ancillaryDataProducts = await Api.getAncillaryDataProducts();
      expect(ancillaryDataProducts).toBeTruthy();
    });
  });

  describe("getActivity", async () => {
    it("should return something truthy on successful call", async () => {
      fetchMocker.mockResponse(
        createFetchResponse<Activity>(
          {
            id: 123,
            name: "test",
            type: "none",
            url: "https://example.com",
            version: "1",
            collection: "",
            parents: [],
            ra: 0,
            dec: 0,
          },
          200
        )
      );

      const activity = await Api.getActivity("1");
      expect(activity).toBeTruthy();
    });

    it("should throw an error on HTTP 500", async () => {
      fetchMocker.mockResponse(
        createFetchResponse<object>(
          {
            detail: "Kaput",
          },
          500
        )
      );

      expect(Api.getActivity("1")).rejects.toEqual(
        Error("Internal Server Error")
      );
    });

    it("should throw an error on HTTP 404", async () => {
      fetchMocker.mockResponse(
        createFetchResponse<object>(
          {
            detail: "No such activity",
          },
          404
        )
      );

      expect(Api.getActivity("1")).rejects.toEqual(Error("Not Found"));
    });

    it("should throw an error when passed an invalid activityId", async () => {
      expect(Api.getActivity(undefined)).rejects.toEqual(
        TypeError("Invalid activityId: undefined")
      );
    });
  });
});
