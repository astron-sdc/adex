import { render } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import { vi } from "vitest";
import SkyView from "../components/SkyView/SkyView";
import SkyCoords from "../utils/SkyCoords.ts";

// eslint-disable-next-line
const dummyRaDec = (_: SkyCoords) => {};
// eslint-disable-next-line
const dummyFov = (_: number) => {};

// Aladin-lite could in theory not resolve, make sure that is handled gracefully.
vi.mock("aladin-lite", async () => {
  const aladin = await import("./SkyView.mock.ts");
  return { default: aladin.AladinStub };
});

describe("SkyView", async () => {
  it("renders correctly when it does not resolve", async () => {
    await act(async () => {
      const container = render(
        <SkyView
          primaryDataProducts={[]}
          coords={{ ra: 0, dec: 0 }}
          updateCoords={dummyRaDec}
          fov={0}
          updateFov={dummyFov}
          survey={null}
        />
      );
      expect(container).toBeTruthy();
    });
  });
});
