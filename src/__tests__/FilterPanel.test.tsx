import { render, screen, waitFor } from "@testing-library/react";
import { vi } from "vitest";
import FilterPanel from "../components/FilterPanel/FilterPanel";
import { filterProps, user } from "./utils.ts";

describe("FilterPanel", async () => {
  const collection = [
    { name: "Cartoons", dp_types: ["Tom and Jerry"] },
    { name: "Anime", dp_types: ["One Punch"] },
  ];
  const collectionAll = {
    name: "All",
    dp_types: ["Tom and Jerry", "One Punch", "All"],
  };

  it("renders correctly", async () => {
    const container = render(FilterPanel(filterProps));
    expect(container).toBeTruthy();
  });

  it("can filter by all collections", async () => {
    const spyCollection = vi.spyOn(filterProps, "updateCollection");
    render(
      FilterPanel({
        ...filterProps,
        collections: [...collection, collectionAll],
        state: {
          collection: collection[0],
          dp_type: collection[0].dp_types[0],
        },
      })
    );

    const collectionInput = screen.getAllByRole("combobox")[0];
    await user.type(collectionInput, "Al");
    const allCollections = screen.getByText("All");
    expect(allCollections).toBeVisible();
    await user.click(allCollections);
    await waitFor(() => {
      expect(spyCollection).toHaveBeenCalledWith(collectionAll);
    });
    expect(collectionInput).toHaveValue("All");
  });

  it("can filter by all dp types", async () => {
    const spyDpType = vi.spyOn(filterProps, "updateDpType");
    render(
      FilterPanel({
        ...filterProps,
        collections: [collectionAll, ...collection],
        state: {
          collection: collectionAll,
          dp_type: collectionAll.dp_types[0], // "Tom and Jerry"
        },
      })
    );
    const dpTypeInput = screen.getAllByRole("combobox")[1];
    expect(dpTypeInput).toHaveValue("Tom and Jerry");

    await user.type(dpTypeInput, "Al");
    const allDpTypes = screen.getByText("All");
    expect(allDpTypes).toBeVisible();
    await user.click(allDpTypes);
    await waitFor(() => {
      expect(spyDpType).toHaveBeenCalledWith("All");
    });
    expect(dpTypeInput).toHaveValue("All");
  });
});
