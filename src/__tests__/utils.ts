import { MockResponseInitFunction } from "vitest-fetch-mock/types";
import FilterProps from "../components/FilterPanel/FilterProps.ts";
import SurveyConfiguration from "../api/models/SurveyConfiguration";
import SkyCoords from "../utils/SkyCoords.ts";
import Collection from "../api/models/Collection.ts";
import userEvent from "@testing-library/user-event";

/**
 * Utility for creating fetch Responses
 * @param data
 * @returns
 */
export function createFetchResponse<T>(
  data: T,
  status = 200
): MockResponseInitFunction {
  return () => ({ body: JSON.stringify(data), init: { status: status } });
}

export const user = userEvent.setup();

/* eslint-disable */
const dummySetCoords = (_: SkyCoords) => {};
const dummySetCollection = (_: Collection | null) => {};
const dummySetNumber = (_: number) => {};
const dummySetString = (_: string | null) => {};
const dummySurvey = (_: SurveyConfiguration | null) => {};
/* eslint-enable */

export const filterProps: FilterProps = {
  skyviewConfiguration: undefined,
  survey: null,
  setSurvey: dummySurvey,
  collections: [],
  state: {
    collection: null,
    dp_type: null,
  },
  updateCollection: dummySetCollection,
  updateDpType: dummySetString,
  fov: 60,
  coords: { ra: 0, dec: 0 },
  updateCoords: dummySetCoords,
  updateFov: dummySetNumber,
  targetName: "",
  updateTargetName: dummySetString,
  nameResolveStatus: "",
  updateNameResolveStatus: dummySetString,
};
