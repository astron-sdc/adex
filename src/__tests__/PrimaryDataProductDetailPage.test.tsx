import { render } from "@testing-library/react";
import PrimaryDataProductDetailPage from "../pages/PrimaryDataProductDetailPage/PrimaryDataProductDetailPage";
import PrimaryDataProduct from "../api/models/PrimaryDataProduct";
import * as ReactRouter from "react-router";
import { vi } from "vitest";
import { MemoryRouter } from "react-router-dom";

describe("PrimaryDataProductDetailPage", async () => {
  it("renders correctly", async () => {
    vi.spyOn(ReactRouter, "useLoaderData").mockImplementation(
      vi.fn<unknown[], PrimaryDataProduct>().mockReturnValue({
        id: 1,
        pid: "",
        dp_type: "",
        ra: 0,
        dec: 0,
        activity: "",
        access_url: "",
      })
    );

    const container = render(
      <MemoryRouter>
        <PrimaryDataProductDetailPage />
      </MemoryRouter>
    );

    expect(container).toBeTruthy();
  });
});
