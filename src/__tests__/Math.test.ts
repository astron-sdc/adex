import {
  floatCompare,
  isInputWithinLimits,
  roundNumber,
} from "../utils/Math.ts";

describe("isInputWithinLimits", () => {
  it.each([
    [1, 0.9999999, 1.0000001],
    [-1, -1.0000001, -0.9999999],
  ])(
    "should return true when `value` is within the limits",
    (value, min, max) => {
      const result = isInputWithinLimits(value, min, max);
      expect(result).toBe(true);
    }
  );

  it("should return true when the `value` is undefined", () => {
    const result = isInputWithinLimits(undefined, -0.9999999, 1.0000001);
    expect(result).toBe(true);
  });

  it.each([
    [1.0000001, 0.9999999, 1],
    [1.0000001, -1, -0.9999999],
  ])(
    "should return false when `value` is not within the limits",
    (value, min, max) => {
      const result = isInputWithinLimits(value, min, max);
      expect(result).toBe(false);
    }
  );

  it("should throw an error when the limits are incorrect", () => {
    const result = () => isInputWithinLimits(1, 1.0000001, 0.9999999);
    expect(result).toThrowError("min must be smaller than max");
  });
});

describe("roundDecimal", () => {
  it.each([
    [1.9999999, "2.00"],
    [2, "2.00"],
    [1.4449, "1.44"],
  ])(
    "should round correctly with default decimalPlaces when numberToRound is a number",
    (numberToRound, expected) => {
      expect(roundNumber(numberToRound)).toBe(expected);
    }
  );

  it.each([
    [1.9999999, 7, "1.9999999"],
    [2.05, 0, "2"],
    [1.4449, 3, "1.445"],
  ])(
    "should round correctly with different decimalPlacess when numberToRound is a number",
    (numberToRound, decimalPlaces, expected) => {
      expect(roundNumber(numberToRound, decimalPlaces)).toBe(expected);
    }
  );

  it.each([undefined, null])(
    "should return undefined when it is undefined or null",
    (numberToRound) => {
      expect(roundNumber(numberToRound)).toBeUndefined();
      expect(roundNumber(numberToRound, 0)).toBeUndefined();
    }
  );
});

describe("floatCompare", () => {
  it.each([
    [1, 1, true],
    [0, 0, true],
    [42.123, 42.123, true],
    [1.1, 1.1, true],
    [1, 1.0000001, false],
    [-1, -1.0000001, false],
    [1, 0, false],
    [0, 1, false],
  ])("should compare numbers correctly", (a, b, expected) => {
    const result = floatCompare(a, b);
    expect(result).toBe(expected);
  });
});
