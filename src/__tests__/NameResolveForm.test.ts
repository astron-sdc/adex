import createFetchMock from "vitest-fetch-mock";
import { vi } from "vitest";
import { createFetchResponse, filterProps, user } from "./utils.ts";
import {
  SkyCoordinates,
  SkyCoordinatesError,
} from "../api/models/GetSkyCoordinates.ts";
import { render, screen, waitFor } from "@testing-library/react";
import { NameResolveForm } from "../components/FilterPanel/NameResolveForm.tsx";

describe("NameResolveForm", async () => {
  const fetchMocker = createFetchMock(vi);
  beforeEach(() => fetchMocker.enableMocks());
  afterEach(() => fetchMocker.resetMocks());

  const successMock = () => {
    fetchMocker.mockResponse(
      createFetchResponse<SkyCoordinates>(
        {
          description: "",
          target_name: "m87",
          ra: "187.7059308",
          dec: "12.3911232",
        },
        200
      )
    );
  };

  const errorMock = () => {
    fetchMocker.mockResponse(
      createFetchResponse<SkyCoordinatesError>(
        {
          error: "no target_name given",
        },
        404
      )
    );
  };

  it("should render correctly", () => {
    render(NameResolveForm(filterProps));

    const input = screen.getByLabelText("Target Name");
    expect(input).toBeTruthy();
    const button = screen.getByRole("button");
    expect(button).toBeTruthy();
  });

  it("should set the coordinates found after button click", async () => {
    successMock();
    const spy = vi.spyOn(filterProps, "updateCoords");
    render(NameResolveForm(filterProps));
    const button = screen.getByRole("button");
    await user.click(button);
    await waitFor(() => {
      expect(spy).toHaveBeenCalledWith({ ra: 187.7059308, dec: 12.3911232 });
    });
  });

  it("should set the coordinates found after enter key", async () => {
    successMock();
    const spy = vi.spyOn(filterProps, "updateCoords");
    render(NameResolveForm(filterProps));
    const input = screen.getByLabelText("Target Name");
    await user.type(input, "{enter}");
    await waitFor(() => {
      expect(spy).toHaveBeenCalled();
    });
  });

  it.each([
    ["Error resolving target name.", errorMock],
    ["Coordinates found.", successMock],
  ])(
    "should display status messages correctly",
    async (statusMessage, mock) => {
      mock();
      const spy = vi.spyOn(filterProps, "updateNameResolveStatus");
      render(NameResolveForm(filterProps));

      const button = screen.getByRole("button");
      await user.click(button);
      await waitFor(() => {
        expect(spy).toBeCalledWith("Loading..."); // loading
        expect(spy).toBeCalledWith(statusMessage); // error or success
      });
    }
  );

  it("should reset the status message on input change", async () => {
    successMock();
    const spy = vi.spyOn(filterProps, "updateNameResolveStatus");
    render(NameResolveForm(filterProps));
    const input = screen.getByLabelText("Target Name");

    // setup previous state
    await user.type(input, "{enter}");
    await waitFor(() => {
      expect(spy).toBeCalledWith("Loading...");
      expect(spy).toBeCalledWith("Coordinates found.");
    });

    // state to be tested
    await user.type(input, "{a}");
    await waitFor(() => {
      expect(spy).toBeCalledWith(""); // awaiting input
    });
  });
});
