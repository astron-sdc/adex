import { render } from "@testing-library/react";
import TableView from "../components/TableView/TableView";
import PrimaryDataProduct from "../api/models/PrimaryDataProduct";
import { MemoryRouter } from "react-router-dom";

describe("TableView", async () => {
  it("renders correctly", async () => {
    const container = render(<TableView primaryDataProducts={[]} />);
    expect(container).toBeTruthy();
  });

  it("displays a row for each data product (ignoring pagination)", async () => {
    const dataProducts = [
      { pid: "1", dp_type: "DataProductRow" } as PrimaryDataProduct,
      { pid: "2", dp_type: "DataProductRow" } as PrimaryDataProduct,
      { pid: "3", dp_type: "DataProductRow" } as PrimaryDataProduct,
    ];

    const container = render(<TableView primaryDataProducts={dataProducts} />);
    const rows = container.queryAllByText("DataProductRow");
    expect(rows.length).toBe(dataProducts.length);
  });

  it("shows the 'View Process' action button in each row when 'showLinkToProcessDetail' is set to true", async () => {
    const dataProducts = [
      {
        pid: "1",
        dp_type: "DataProductRow",
        activity: "fun",
      } as PrimaryDataProduct,
      {
        pid: "2",
        dp_type: "DataProductRow",
        activity: "fun",
      } as PrimaryDataProduct,
    ];

    const container = render(
      <MemoryRouter>
        <TableView
          primaryDataProducts={dataProducts}
          showLinkToProcessDetail={true}
        />
      </MemoryRouter>
    );

    const links = container.queryAllByTestId("processDetailLink");
    expect(links.length).toBe(dataProducts.length);
  });

  it("shows the 'View data product details' action button in each row when 'showLinkToDataProductDetail' is set to true", async () => {
    const dataProducts = [
      {
        id: 1,
        pid: "1",
        dp_type: "DataProductRow",
        activity: "fun",
      } as PrimaryDataProduct,
      {
        id: 2,
        pid: "2",
        dp_type: "DataProductRow",
        activity: "fun",
      } as PrimaryDataProduct,
    ];

    const container = render(
      <MemoryRouter>
        <TableView
          primaryDataProducts={dataProducts}
          showLinkToDataProductDetail={true}
        />
      </MemoryRouter>
    );

    const links = container.queryAllByTestId("dataProductDetailLink");
    expect(links.length).toBe(dataProducts.length);
  });
});
