import PrimaryDataProduct from "../../api/models/PrimaryDataProduct";

interface TableViewProps {
  primaryDataProducts: PrimaryDataProduct[];
  showLinkToProcessDetail?: boolean;
  showLinkToDataProductDetail?: boolean;
}

export default TableViewProps;
