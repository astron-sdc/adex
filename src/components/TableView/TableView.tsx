import { useMemo } from "react";
import styles from "./TableView.module.css";
import TableViewProps from "./TableViewProps";
import { MaterialReactTable, type MRT_ColumnDef } from "material-react-table";
import PrimaryDataProduct from "../../api/models/PrimaryDataProduct";
import ColumnDefinitions from "./ColumnDefinitions";
import { Button } from "@mui/material";
import { Link } from "react-router-dom";
import { downloadPrimaryDataProducts } from "../../api/utils/PrimaryDataProduct";

const TableView = (props: TableViewProps) => {
  const columns = useMemo<MRT_ColumnDef<PrimaryDataProduct>[]>(
    () => ColumnDefinitions,
    []
  );

  const linkToProcessDetail = (dataProductId: number, activity: string) =>
    props.showLinkToProcessDetail && activity ? (
      <Link
        key={`process_link_${dataProductId}`}
        data-testid="processDetailLink"
        to={`/process/${activity}`}
      >
        <Button>View process</Button>
      </Link>
    ) : null;

  const linkToDataProductDetail = (dataProductId: number) =>
    props.showLinkToDataProductDetail && dataProductId ? (
      <Link
        key={`dataproduct_link_${dataProductId}`}
        data-testid="dataProductDetailLink"
        to={`/dataproduct/${dataProductId}`}
      >
        <Button>View data product</Button>
      </Link>
    ) : null;

  return (
    <div className={styles.skyView}>
      <h2>Products</h2>
      <MaterialReactTable
        initialState={{ density: "compact" }}
        columns={columns}
        data={props.primaryDataProducts}
        enableGlobalFilter={false}
        enableColumnOrdering={true}
        enableRowActions={true}
        positionActionsColumn="last"
        enableRowSelection={true}
        renderRowActions={({ row }) => [
          linkToProcessDetail(row.original.id, row.original.activity),
          linkToDataProductDetail(row.original.id),
        ]}
        renderTopToolbarCustomActions={({ table }) => {
          return (
            <div>
              <Button
                color="info"
                disabled={!table.getIsSomeRowsSelected()}
                onClick={async () => {
                  await downloadPrimaryDataProducts(
                    table
                      .getSelectedRowModel()
                      .flatRows.map((flatRow) => flatRow.original),
                    document
                  );

                  table.resetRowSelection();
                }}
                variant="contained"
              >
                Download data products
              </Button>
            </div>
          );
        }}
      />
    </div>
  );
};

export default TableView;
