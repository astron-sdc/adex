import { MRT_ColumnDef } from "material-react-table";
import PrimaryDataProduct from "../../api/models/PrimaryDataProduct";
import { roundNumber } from "../../utils/Math.ts";

const columnDefinitions: MRT_ColumnDef<PrimaryDataProduct>[] = [
  {
    header: "Pid",
    accessorKey: "pid",
    minSize: 50,
    size: 50,
  },
  {
    header: "Data Product Type",
    accessorKey: "dp_type",
  },
  {
    header: "RA",
    accessorKey: "ra",
    minSize: 50,
    size: 50,
    Cell: ({ renderedCellValue }) => roundNumber(renderedCellValue as number),
  },
  {
    header: "DEC",
    accessorKey: "dec",
    minSize: 50,
    size: 50,
    Cell: ({ renderedCellValue }) => roundNumber(renderedCellValue as number),
  },
];

export default columnDefinitions;
