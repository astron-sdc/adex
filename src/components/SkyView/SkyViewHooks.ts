import AladinLite, { AladinInstance } from "aladin-lite";
import { useEffect, useRef, useState } from "react";
import PrimaryDataProduct from "../../api/models/PrimaryDataProduct";
import SurveyConfiguration from "../../api/models/SurveyConfiguration";
import SkyCoords from "../../utils/SkyCoords.ts";
import { floatCompare } from "../../utils/Math";

/**
 * Hook the update the AladinInstance with the given coords on updates
 * Prevents updates when the coords are exactly the same
 * @param instance
 * @param coords
 */
export const useCoords = (
  instance: AladinInstance | null,
  coords: SkyCoords
) => {
  if (instance) {
    // If old == new, then don't set them
    // this prevents rotating the view when you move the view in aladin-lite
    const old = instance.getRaDec();
    if (floatCompare(old[0], coords.ra) && floatCompare(old[1], coords.dec))
      return;

    instance.gotoRaDec(coords.ra, coords.dec);
  }
};

/**
 * Hook to update the AladinInstance with the given fov on updates
 * Prevents updates when the coords are exactly the same
 * @param instance
 * @param fov
 */
export const useFov = (instance: AladinInstance | null, fov: number) => {
  useEffect(() => {
    if (instance) {
      const old = instance.getFov()[0];
      if (floatCompare(old, fov)) return;
      instance.setFov(fov);
    }
  }, [fov, instance]);
};

/**
 * Hook to update the catalog layer in the given AladinInstance
 * @param instance
 * @param primaryDataProducts
 */
export const useDataProducts = (
  instance: AladinInstance | null,
  primaryDataProducts: PrimaryDataProduct[]
) => {
  useEffect(() => {
    if (instance) {
      instance.removeLayers();
      const sources = primaryDataProducts.map((dp) =>
        AladinLite.source(dp.ra, dp.dec, dp)
      );

      const catalog = AladinLite.catalog({
        name: "Data Products",
        color: "#FF0",
        onClick: "showPopup",
      });
      catalog.addSources(sources);
      instance.addCatalog(catalog);
    }
  }, [instance, primaryDataProducts]);
};

/**
 * Hook to update the BaseImage Layer of the given AladinInstance on updates
 * @param instance
 * @param survey
 */
export const useBackgroundSurvey = (
  instance: AladinInstance | null,
  survey: SurveyConfiguration | null
) => {
  useEffect(() => {
    // TODO: look into caching creation of imageSurveys?
    if (instance && survey) {
      const s = instance.createImageSurvey(
        survey.id,
        survey.name,
        survey.url,
        null,
        null,
        { imgFormat: survey.format }
      );
      instance.setBaseImageLayer(s);
    }
  }, [instance, survey]);
};

/**
 * Adds the given zoom event handler to the drawing canvas and the zoom buttons
 * Returns a function to remove the eventListeners
 * @param el the Main HTMLDivElement hosting the aladin-lite view.
 * @param handler
 * @returns function to remove the eventListeners
 */
const addZoomHandler = (el: HTMLDivElement, handler: () => void) => {
  // Catch wheel events at the canvas, since aladin 'captures` the event
  const canvasEls = el.getElementsByClassName("aladin-catalogCanvas");
  const canvas = canvasEls[0];

  // Attach zoom handler too to the zoom buttons.
  const zoomPlusEls = el.getElementsByClassName("zoomPlus");
  const zoomPlus = zoomPlusEls[0];

  const zoomMinusEls = el.getElementsByClassName("zoomMinus");
  const zoomMinus = zoomMinusEls[0];

  canvas.addEventListener("wheel", handler);
  zoomPlus.addEventListener("click", handler);
  zoomMinus.addEventListener("click", handler);

  return () => {
    canvas.removeEventListener("wheel", handler);
    zoomPlus.removeEventListener("click", handler);
    zoomMinus.removeEventListener("click", handler);
  };
};

/**
 * Hook to keep track of the Div reference and main AladinInstance
 *
 * @param init check for if the aladin-lite web-assembly has been initialized
 * @param updateFov setStateFunction used for zoom event handling
 * @returns a React reference for the div element and an AladinInstance object
 */
export const useAladinInstance = (
  init: boolean,
  updateFov: (fov: number) => void
) => {
  const ref = useRef<HTMLDivElement>(null);
  const [instance, setInstance] = useState<AladinInstance | null>(null);

  // Constructor and destructor the Aladin instance.
  useEffect(() => {
    // Curried Aladin instance to prevent useEffect dependency
    // on `instance` state.
    const myZoomHandler = (a: AladinInstance) => () => {
      const fov = a.getFov();
      updateFov(fov[0]);
    };

    // Element exists and `A` is available
    if (ref.current && init) {
      const el = ref.current;
      const a = AladinLite.aladin(`#${el.id}`, {
        cooFrame: "ICRSd",
      });

      const handler = myZoomHandler(a);
      const removeListeners = addZoomHandler(el, handler);
      setInstance(a);

      // Destroy Aladin SkyView
      return () => {
        removeListeners();
        setInstance(null);
        el.textContent = null;
      };
    }
  }, [init, updateFov]); // note that updateFov should be stable reference

  return { ref: ref, instance: instance };
};

/**
 * Hook for checking if aladin-lite web-assembly is initialized or not
 * @returns boolean for when the aladin-lite web-assembly has been initialized.
 */
export const useAladinInit = () => {
  const [init, setInit] = useState(false);
  // Only initialize the web assembly once
  useEffect(() => {
    AladinLite.init
      .then(() => {
        setInit(true);
      })
      .catch((err) => {
        console.error(err);
      });
  }, []);
  return init;
};
