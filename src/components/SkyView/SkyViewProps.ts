import PrimaryDataProduct from "../../api/models/PrimaryDataProduct";
import SurveyConfiguration from "../../api/models/SurveyConfiguration";
import SkyCoords from "../../utils/SkyCoords.ts";

interface SkyViewProps {
  primaryDataProducts: PrimaryDataProduct[];
  coords: SkyCoords;
  updateCoords: (coords: SkyCoords) => void;
  fov: number;
  updateFov: (fov: number) => void; // must be a stable reference; prefer `setState` functions directly over component defined functions
  survey: SurveyConfiguration | null;
}

export default SkyViewProps;
