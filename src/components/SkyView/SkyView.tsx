import styles from "./SkyView.module.css";
import {
  useAladinInit,
  useAladinInstance,
  useBackgroundSurvey,
  useCoords,
  useDataProducts,
  useFov,
} from "./SkyViewHooks";
import SkyViewProps from "./SkyViewProps";

const SkyView = (props: SkyViewProps) => {
  const { coords, updateCoords, fov, updateFov, primaryDataProducts, survey } =
    props;

  const init = useAladinInit();
  const { ref, instance } = useAladinInstance(init, updateFov);
  useDataProducts(instance, primaryDataProducts);
  useCoords(instance, coords);
  useFov(instance, fov);
  useBackgroundSurvey(instance, survey);

  const handleMove = () => {
    if (instance) {
      const [ra, dec] = instance.getRaDec();
      updateCoords({ ra: ra, dec: dec });
    }
  };

  if (!init) return <div className={styles.skyView}>Loading Sky View...</div>;
  return (
    <div className={styles.skyView}>
      <div
        onMouseLeave={handleMove}
        onMouseUp={handleMove}
        role="none" // requires for a11y purposes
        ref={ref}
        id="SkyView"
      />
    </div>
  );
};

export default SkyView;
