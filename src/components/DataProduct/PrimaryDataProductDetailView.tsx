import { Link as RouterLink } from "react-router-dom";
import PrimaryDataProduct from "../../api/models/PrimaryDataProduct";
import styles from "./PrimaryDataProductDetailView.module.css";
import { Link, Table, TableBody, TableCell, TableRow } from "@mui/material";
import { roundNumber } from "../../utils/Math.ts";

const PrimaryDataProductDetailView = (props: {
  primaryDataProduct: PrimaryDataProduct;
}) => (
  <div className={styles.primaryDataProductDetailsPanel}>
    <Table>
      <TableBody>
        <TableRow>
          <TableCell>
            <strong>Pid</strong>
          </TableCell>
          <TableCell>{props.primaryDataProduct.pid}</TableCell>
        </TableRow>
        <TableRow>
          <TableCell>
            <strong>Data Product Type</strong>
          </TableCell>
          <TableCell>{props.primaryDataProduct.dp_type}</TableCell>
        </TableRow>
        <TableRow>
          <TableCell>
            <strong>RA</strong>
          </TableCell>
          <TableCell>{roundNumber(props.primaryDataProduct.ra)}</TableCell>
        </TableRow>
        <TableRow>
          <TableCell>
            <strong>Dec</strong>
          </TableCell>
          <TableCell>{roundNumber(props.primaryDataProduct.dec)}</TableCell>
        </TableRow>
        <TableRow>
          <TableCell>
            <strong>Activity</strong>
          </TableCell>
          <TableCell>
            <RouterLink
              data-testid="processDetailLink"
              to={`/process/${props.primaryDataProduct.activity}`}
            >
              {props.primaryDataProduct.activity}
            </RouterLink>
          </TableCell>
        </TableRow>
      </TableBody>
    </Table>
    <Link href={props.primaryDataProduct.access_url}>
      Download Data Product
    </Link>
  </div>
);

export default PrimaryDataProductDetailView;
