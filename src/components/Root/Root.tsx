import { Typography } from "@astron-sdc/design-system";
import { StyledEngineProvider } from "@mui/material/styles";
import { QueryClient, QueryClientProvider } from "react-query";
import { Link, Outlet } from "react-router-dom";
import styles from "./Root.module.css";

const Root = () => {
  const queryClient = new QueryClient();

  return (
    <StyledEngineProvider injectFirst>
      <div className={styles.rootContainer}>
        <div className={styles.header}>
          <Link to="/">
            <Typography variant="h1" text="ASTRON Data Explorer" />
          </Link>
        </div>
        <div className={styles.content}>
          <QueryClientProvider client={queryClient}>
            <Outlet />
          </QueryClientProvider>
        </div>
      </div>
    </StyledEngineProvider>
  );
};

export default Root;
