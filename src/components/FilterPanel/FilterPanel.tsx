import { Autocomplete, InputAdornment, TextField } from "@mui/material";
import { ChangeEvent } from "react";
import { NumberFormatValues, NumericFormat } from "react-number-format";
import Collection from "../../api/models/Collection";
import { isInputWithinLimits } from "../../utils/Math.ts";
import SurveySelect from "../SurveySelect/SurveySelect.tsx";
import styles from "./FilterPanel.module.css";
import FilterProps from "./FilterProps";
import { NameResolveForm } from "./NameResolveForm.tsx";
import { Typography } from "@astron-sdc/design-system";

const FilterPanel = (props: FilterProps) => {
  const isAllowedRa = (values: NumberFormatValues) =>
    isInputWithinLimits(values.floatValue, 0, 360);

  const isAllowedDec = (values: NumberFormatValues) =>
    isInputWithinLimits(values.floatValue, -90, 90);

  const isAllowedFov = (values: NumberFormatValues) =>
    isInputWithinLimits(values.floatValue, 0, 180);

  const handleRa = (evt: ChangeEvent<HTMLInputElement>) => {
    props.updateCoords({
      ra: parseFloat(evt.target.value),
      dec: props.coords.dec,
    });
  };

  const handleDec = (evt: ChangeEvent<HTMLInputElement>) => {
    props.updateCoords({
      ra: props.coords.ra,
      dec: parseFloat(evt.target.value),
    });
  };

  const handleFov = (evt: ChangeEvent<HTMLInputElement>) => {
    props.updateFov(parseFloat(evt.target.value));
  };

  return (
    <div className={styles.filterPanel}>
      <Typography variant="h4" text="Filters" />
      {/* TODO: add extra margin bottom to title */}
      <div className={styles.filterForm}>
        <Autocomplete
          onChange={(_: unknown, collection: Collection | null) => {
            props.updateCollection(collection);
          }}
          options={props.collections}
          getOptionLabel={(collection) => collection.name}
          isOptionEqualToValue={(option, value) => {
            return option.name === value.name;
          }}
          value={props.state.collection}
          renderInput={(params) => <TextField {...params} label="Collection" />}
        />
        <Autocomplete
          onChange={(_, dpType: string | null) => {
            props.updateDpType(dpType);
          }}
          options={props?.state?.collection?.dp_types ?? []}
          getOptionLabel={(dp_type) => dp_type}
          isOptionEqualToValue={(option, value) => {
            return option === value;
          }}
          value={props.state.dp_type}
          renderInput={(params) => (
            <TextField {...params} label="Data Product Type" />
          )}
        />

        <SurveySelect
          availableSurveys={props.skyviewConfiguration?.surveys}
          survey={props.survey}
          setSurvey={props.setSurvey}
        />

        {NameResolveForm(props)}

        <Typography variant="note" text="Or input coordinates manually:" />
        <div className={styles.coordinates}>
          <NumericFormat
            label="RA"
            value={props.coords.ra}
            onChange={handleRa}
            decimalScale={7}
            customInput={TextField}
            InputProps={{
              endAdornment: <InputAdornment position="end">deg</InputAdornment>,
            }}
            isAllowed={isAllowedRa}
          />
          <NumericFormat
            label="DEC"
            value={props.coords.dec}
            onChange={handleDec}
            decimalScale={7}
            customInput={TextField}
            InputProps={{
              endAdornment: <InputAdornment position="end">deg</InputAdornment>,
            }}
            isAllowed={isAllowedDec}
          />
        </div>
        <NumericFormat
          label="FOV"
          value={props.fov}
          onChange={handleFov}
          decimalScale={2}
          customInput={TextField}
          InputProps={{
            endAdornment: <InputAdornment position="end">deg</InputAdornment>,
          }}
          isAllowed={isAllowedFov}
        />
      </div>
    </div>
  );
};

export default FilterPanel;
