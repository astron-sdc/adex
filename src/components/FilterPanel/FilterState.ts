import Collection from "../../api/models/Collection";

interface FilterState {
  collection: Collection | null;
  dp_type: string | null;
}

export default FilterState;
