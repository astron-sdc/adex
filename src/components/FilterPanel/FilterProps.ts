import Collection from "../../api/models/Collection";
import SkyViewConfiguration from "../../api/models/SkyViewConfiguration";
import SurveyConfiguration from "../../api/models/SurveyConfiguration";
import SkyCoords from "../../utils/SkyCoords.ts";
import FilterState from "./FilterState";

interface FilterProps {
  collections: Collection[];
  skyviewConfiguration?: SkyViewConfiguration;
  survey: SurveyConfiguration | null;
  state: FilterState;
  targetName: string;
  nameResolveStatus: string;
  coords: SkyCoords;
  fov: number;
  updateCollection: (collection: Collection | null) => void;
  updateDpType: (dpType: string | null) => void;
  updateTargetName: (targetName: string) => void;
  updateNameResolveStatus: (nameResolveStatus: string) => void;
  updateCoords: (coords: SkyCoords) => void;
  updateFov: (fov: number) => void;
  setSurvey: (survey: SurveyConfiguration | null) => void;
}

export default FilterProps;
