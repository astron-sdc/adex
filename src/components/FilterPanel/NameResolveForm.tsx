import { Button, TextInput } from "@astron-sdc/design-system";
import { Dispatch, SetStateAction } from "react";
import Api from "../../api/Api.ts";
import { SkyCoordinates } from "../../api/models/GetSkyCoordinates.ts";
import styles from "./FilterPanel.module.css";
import FilterProps from "./FilterProps.ts";

export const NameResolveForm = (props: FilterProps) => {
  const handleNameInput: Dispatch<SetStateAction<string>> = (value) => {
    props.updateNameResolveStatus("");
    props.updateTargetName(value as string);
  };

  const handleNameResolve = async () => {
    props.updateNameResolveStatus("Loading...");
    try {
      const nameResolveCoords = await Api.getCoordsByTargetName(
        props.targetName
      );
      props.updateNameResolveStatus("Coordinates found.");
      props.updateCoords({
        ra: parseFloat((nameResolveCoords as SkyCoordinates).ra),
        dec: parseFloat((nameResolveCoords as SkyCoordinates).dec),
      });
    } catch (e) {
      props.updateNameResolveStatus("Error resolving target name.");
    }
  };

  return (
    <form
      className={styles.nameResolveForm}
      onSubmit={async (e) => {
        e.preventDefault();
        e.stopPropagation();
        await handleNameResolve();
      }}
    >
      <TextInput
        label="Target Name"
        value={props.targetName}
        onValueChange={handleNameInput}
      />
      <div>
        <Button label="Resolve" onClick={handleNameResolve} />
        <div>{props.nameResolveStatus}</div>
      </div>
    </form>
  );
};
