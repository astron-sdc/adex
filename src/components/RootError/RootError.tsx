/**
 *  Copyright 2023 ASTRON
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { Card, CardContent, CardHeader } from "@mui/material";
import { useRouteError } from "react-router-dom";
import styles from "./RootError.module.css";

/**
 * Error boundary in the root, which displays a message in case of an unrecoverable error.
 */
const RootError = () => {
  // Allow any error to be caught
  // eslint-disable-next-line
  let err: any = useRouteError();

  // try for err.error is an Error
  if (!(err instanceof Error)) {
    err = err.error;
  }

  return (
    <Card className={styles.card}>
      <CardHeader
        data-testid="errorMessage"
        title="Awww Snap; Something went wrong."
      />
      <CardContent>
        <h3>{err.name}</h3>
        <pre>{err.message}</pre>
      </CardContent>
    </Card>
  );
};

export default RootError;
