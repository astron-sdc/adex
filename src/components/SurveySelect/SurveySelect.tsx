import { Autocomplete, TextField } from "@mui/material";
import { useState } from "react";
import SurveyConfiguration from "../../api/models/SurveyConfiguration";
import SurveySelectProps from "./SurveyProps";

const SurveySelect = (props: SurveySelectProps) => {
  const [input, setInput] = useState(""); // required for making Autocomplete controlled

  const { availableSurveys } = props;
  if (!availableSurveys) return null;

  const allSurveys: SurveyConfiguration[] = Object.keys(
    availableSurveys
  ).flatMap((key) => availableSurveys[key]);

  return (
    <Autocomplete
      id="survey-select"
      options={allSurveys}
      groupBy={(option) => new URL(option.url).hostname} // surveys are grouped by the hostname of their URL
      isOptionEqualToValue={(o, v) => o.id === v.id}
      getOptionLabel={(option) => option.name}
      value={props.survey}
      onChange={(_, value) => props.setSurvey(value)}
      inputValue={input}
      onInputChange={(_, e) => setInput(e)}
      renderInput={(params) => (
        <TextField {...params} label="Background Survey" />
      )}
    />
  );
};

export default SurveySelect;
