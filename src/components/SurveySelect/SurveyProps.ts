import SurveyConfiguration from "../../api/models/SurveyConfiguration";

interface SurveySelectProps {
  availableSurveys?: { [key: string]: SurveyConfiguration[] } | null;
  survey: SurveyConfiguration | null;
  setSurvey: (survey: SurveyConfiguration | null) => void;
}

export default SurveySelectProps;
