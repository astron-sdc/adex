async function fetchAndReadJson(uri: string) {
  const response = await fetch(uri);
  if (!response.ok) throw Error(response.statusText);
  return await response.json();
}

export { fetchAndReadJson };
