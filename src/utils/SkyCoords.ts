export interface SkyCoords {
  ra: number;
  dec: number;
}

export default SkyCoords;
