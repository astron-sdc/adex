export const isInputWithinLimits = (
  value: number | undefined,
  min: number,
  max: number
) => {
  if (min > max) throw Error("min must be smaller than max");
  // for form inputs before submitting, empty values are allowed
  if (value === undefined) return true;
  return min <= value && value <= max;
};

/**
 *
 * @param numberToRound
 * @param decimalPlaces
 * @returns string representation or undefined it numberToRound is not an number
 */
export const roundNumber = (
  numberToRound: number | undefined | null,
  decimalPlaces = 2
): string | undefined => {
  // returns a rounded string for presentation purposes.
  return numberToRound?.toFixed(decimalPlaces);
};

/**
 * Compare if two floating point numbers are the same
 * @param a left
 * @param b right
 * @param epsilon tolerance between numbers (defaults to 8th digit)
 * @returns whether they are the same or not
 */
export const floatCompare = (
  a: number,
  b: number,
  epsilon = 0.00000001 // Aladin lite uses 7 significant digits
) => {
  return Math.abs(a - b) < epsilon;
};
