function filterEmptyAndWhitespace(array: Array<string>) {
  return array.filter((element) => !!element?.trim());
}

export { filterEmptyAndWhitespace };
