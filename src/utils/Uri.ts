function buildUriFromParams(
  baseUri: string,
  params: object,
  includeNullUndefinedAndEmpty = false
): string {
  const searchParams = new URLSearchParams();

  for (const [key, value] of Object.entries(params)) {
    if (includeNullUndefinedAndEmpty || value || value === 0) {
      searchParams.append(key, value);
    }
  }
  if (baseUri.slice(-1) !== "?") {
    baseUri = baseUri + "?";
  }
  return baseUri + searchParams.toString();
}

export { buildUriFromParams };
