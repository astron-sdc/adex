function downloadFromUrl(document: Document, url: string) {
  const tempFrame = document.createElement("iframe");
  tempFrame.style.display = "none";
  tempFrame.src = url;
  document.body.appendChild(tempFrame);
}

export { downloadFromUrl };
