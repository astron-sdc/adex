/**
 *  Copyright 2023 ASTRON
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import {
  Card,
  CardContent,
  CardHeader,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
} from "@mui/material";
import { useLoaderData } from "react-router";
import Activity from "../../api/models/Activity";

import { useQuery } from "react-query";
import Api from "../../api/Api";
import PrimaryDataProduct from "../../api/models/PrimaryDataProduct";
import TableView from "../../components/TableView/TableView";
import styles from "./ProcessDetail.module.css";
import { roundNumber } from "../../utils/Math.ts";

const ProcessDetailPage = () => {
  const activity = useLoaderData() as Activity;

  // TODO: API does not provide a way from Activity to PrimaryDataProduct yet
  // using hardcoded value for now

  // using `useQuery` here, since we can already show the Activity metadata,
  // but fetching the Data Products might be a long operation
  const { isLoading, data } = useQuery<PrimaryDataProduct[]>(
    ["dataProducts"],
    async () => {
      const response = await Api.getPrimaryDataProducts({
        dataset_id: "190807041",
      });
      return response.results;
    }
  );

  const dataProducts = data ?? [];

  return (
    <Card className={styles.card}>
      <CardHeader title={`Process ${activity.name} (${activity.id})`} />
      <CardContent className={styles.cardContent}>
        <div className={styles.details}>
          <TableContainer component={Paper}>
            <Table>
              <TableBody>
                <TableRow>
                  <TableCell>ID</TableCell>
                  <TableCell>{activity.id}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell>{activity.name}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Type</TableCell>
                  <TableCell>{activity.type}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Collection</TableCell>
                  <TableCell>{activity.collection}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>RA</TableCell>
                  <TableCell>{roundNumber(activity.ra)}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>DEC</TableCell>
                  <TableCell>{roundNumber(activity.dec)}</TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </div>
        <div className={styles.dataProducts}>
          {isLoading ? (
            <span data-testid="loadingMessage">Loading data products</span>
          ) : (
            <TableView
              primaryDataProducts={dataProducts}
              showLinkToDataProductDetail={true}
            />
          )}
        </div>
      </CardContent>
    </Card>
  );
};

export default ProcessDetailPage;
