import { useLoaderData } from "react-router-dom";
import PrimaryDataProduct from "../../api/models/PrimaryDataProduct";
import styles from "./PrimaryDataProductDetailPage.module.css";
import PrimaryDataProductDetailView from "../../components/DataProduct/PrimaryDataProductDetailView";

const PrimaryDataProductDetailPage = () => {
  const primaryDataProduct = useLoaderData() as PrimaryDataProduct;
  return (
    <div className={styles.primaryDataProductDetailPage}>
      <h1>Data Product Details</h1>
      <PrimaryDataProductDetailView primaryDataProduct={primaryDataProduct} />
    </div>
  );
};

export default PrimaryDataProductDetailPage;
