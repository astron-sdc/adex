import { LinearProgress } from "@mui/material";
import { useState } from "react";
import { useQuery } from "react-query";
import Api from "../../api/Api";
import Collection from "../../api/models/Collection";
import SurveyConfiguration from "../../api/models/SurveyConfiguration.ts";
import PrimaryDataProductParams from "../../api/params/PrimaryDataProductParams";
import FilterPanel from "../../components/FilterPanel/FilterPanel";
import FilterState from "../../components/FilterPanel/FilterState";
import SkyView from "../../components/SkyView/SkyView";
import TableView from "../../components/TableView/TableView";
import SkyCoords from "../../utils/SkyCoords.ts";
import styles from "./LandingPage.module.css";

const LandingPage = () => {
  const ALL = "All";

  const [survey, setSurvey] = useState<SurveyConfiguration | null>(null);

  const { isLoading: isConfigurationLoading, data: fetchedConfiguration } =
    useQuery(["configuration"], async () => {
      const response = await Api.getConfiguration();
      const newSkyViewConfiguration = response.configuration.config.skyview;

      // Set the first entry as the default background if none are selected
      if (!survey) {
        const keys = Object.keys(newSkyViewConfiguration.surveys);
        setSurvey(newSkyViewConfiguration.surveys[keys[0]][0]);
      }

      return response.configuration.config;
    });

  const fetchedCollections = fetchedConfiguration?.collections ?? [];
  const selectAll: Collection = {
    name: ALL,
    dp_types: [
      ALL,
      ...fetchedCollections.map((collection) => collection.dp_types).flat(),
    ],
  };
  const collections: Collection[] = [selectAll, ...fetchedCollections];

  const [filter, setFilter] = useState<FilterState>({
    collection: selectAll,
    dp_type: ALL,
  });

  const [targetName, setTargetName] = useState("");
  const [nameResolveStatus, setNameResolveStatus] = useState("");
  const [coords, setCoords] = useState<SkyCoords>({ ra: 4.24, dec: 30.25 });
  const [fov, setFov] = useState(60);

  const dataProductQueryParams = {
    collection:
      filter.collection?.name === ALL ? null : filter.collection?.name,
    dp_type: filter.dp_type === ALL ? null : filter.dp_type,
    ra: coords.ra,
    dec: coords.dec,
    fov: fov,
    distinct_field: filter.collection?.distinct_field,
  } as PrimaryDataProductParams;

  const {
    isLoading: isPrimaryDataProductsLoading,
    data: fetchedPrimaryDataProducts,
  } = useQuery({
    queryKey: ["primaryDataProducts", dataProductQueryParams],
    queryFn: async () => {
      const response = await Api.getPrimaryDataProducts(dataProductQueryParams);
      return response.results;
    },
    keepPreviousData: true,
    staleTime: 60000,
  });

  const primaryDataProducts = fetchedPrimaryDataProducts ?? [];

  const updateCollection = (collection: Collection | null) => {
    if (collection) {
      setFilter((prev) => ({
        ...prev,
        collection: collection,
        dp_type: collection.dp_types[0],
      }));
    }
  };

  const updateDpType = (dpType: string | null) => {
    if (dpType) {
      setFilter((prev) => ({
        ...prev,
        dp_type: dpType,
      }));
    }
  };

  const filterPanel = isConfigurationLoading ? (
    <div data-testid="filterLoader">Loading filters...</div>
  ) : (
    <FilterPanel
      skyviewConfiguration={fetchedConfiguration?.skyview}
      collections={collections}
      state={filter}
      updateCollection={updateCollection}
      updateDpType={updateDpType}
      targetName={targetName}
      updateTargetName={setTargetName}
      nameResolveStatus={nameResolveStatus}
      updateNameResolveStatus={setNameResolveStatus}
      coords={coords}
      survey={survey}
      updateCoords={setCoords}
      fov={fov}
      updateFov={setFov}
      setSurvey={setSurvey}
    />
  );

  return (
    <div className={styles.landingPage}>
      <div className={styles.filter}>{filterPanel}</div>

      <div className={styles.data}>
        <LinearProgress
          data-testid="dataProductLoader"
          sx={{
            visibility: isPrimaryDataProductsLoading ? "visible" : "hidden",
          }}
        />
        <SkyView
          primaryDataProducts={primaryDataProducts}
          coords={coords}
          updateCoords={setCoords}
          fov={fov}
          updateFov={setFov}
          survey={survey}
        />
        <TableView primaryDataProducts={primaryDataProducts} />
      </div>
    </div>
  );
};

export default LandingPage;
